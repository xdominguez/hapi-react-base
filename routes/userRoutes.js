const userController = require("../controllers/userController");
const Joi = require('@hapi/joi');

module.exports = function (server) {
    server.route({
        method: 'POST',
        path: '/api/login',
        config: { auth: false },
        handler: async (request, h) => {
            return await userController.login(request, h);
        }
    });
    server.route({
        method: 'GET',
        path: '/api/user',
        options: {
            auth: "jwt",
            validate: {
                query: Joi.object({
                    page: Joi.number().integer().min(1),
                    perPage: Joi.number().integer().min(1),
                    sortField: Joi.string().valid("firstName", "lastName", "email", "createdAt", ""),
                    sortOrder: Joi.string().valid("ascend", "descend", "")
                })
            }
        },
        handler: async (request, h) => {
            return await userController.list(request, h);
        }
    });
    server.route({
        method: 'GET',
        path: '/api/user/{id}',
        config: { auth: "jwt" },
        handler: async (request, h) => {
            return await userController.get(request, h);
        }
    });
    server.route({
        method: 'POST',
        path: '/api/user',
        handler: async (request, h) => {
            return await userController.create(request, h);
        },
        options: {
            auth: "jwt",
            validate: {
                payload: Joi.object({
                    firstName: Joi.string().max(255).required(),
                    lastName: Joi.string().max(255),
                    email: Joi.string().email().required(),
                    role: Joi.number().integer().min(1).max(2),
                    password: Joi.string().min(8).required(),
                    confirmPassword: Joi.string().min(8).required()
                })
            }
        }
    });
    server.route({
        method: 'PUT',
        path: '/api/user/{id}',
        handler: async (request, h) => {
            return await userController.update(request, h);
        },
        options: {
            auth: "jwt",
            validate: {
                params: Joi.object({
                    id: Joi.string().required()
                }),
                payload: Joi.object({
                    firstName: Joi.string().max(255).required(),
                    lastName: Joi.string().max(255),
                    email: Joi.string().email().required(),
                    role: Joi.number().integer().min(1).max(2),
                    id: Joi.string().required()
                })
            }
        }
    });
    server.route({
        method: 'DELETE',
        path: '/api/user/{id}',
        handler: async (request, h) => {
            return await userController.remove(request, h);
        },
        options: {
            auth: "jwt",
            validate: {
                params: Joi.object({
                    id: Joi.string().required()
                })
            }
        }
    });
};
