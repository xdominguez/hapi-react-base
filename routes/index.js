const userRoutes = require("./userRoutes");

const loadApiRoutes = function (server) {
    userRoutes(server);
};
module.exports = loadApiRoutes;

