module.exports = {
    getRandomString: function(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    },
    extractFilenameFromPath(path){
        return path.replace(/^.*[\\\/]/, '');
    },
    extractExtensionFromPath(path){
        return path.substring(path.lastIndexOf('.')+1, path.length) || path;
    }
};
