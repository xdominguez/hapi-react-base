module.exports = {
    parseList: function (list) {
        return list.map(item => this.parseEntity(item));
    },
    parseEntity: function (user) {
        if(user){
            return {
                id: user.id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                role: user.role,
                photo: user.photo,
                createdAt: user.createdAt,
                updatedAt: user.updatedAt
            }
        }
        return null;
    }
};
