'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    role: DataTypes.INTEGER,
    photo: DataTypes.STRING,
    password: DataTypes.TEXT
  }, {

  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
