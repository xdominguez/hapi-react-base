module.exports = class AppException {
    message = "Oops, something went wrong.";
    code = 500;

    constructor(message, code) {
        this.message = message;
        this.code = code;
    }
};
