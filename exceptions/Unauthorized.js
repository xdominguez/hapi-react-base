const AppException = require("./AppException");
const _ = require("lodash");

module.exports = class Unauthorized extends AppException{
    constructor(message) {
        super(message, 401);
        if(_.isEmpty(message)){
            this.message = "Not authenticated";
        }
    }
};
