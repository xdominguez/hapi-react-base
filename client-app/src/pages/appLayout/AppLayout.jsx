import React, {Component} from "react";
import { Layout, Menu, Breadcrumb, Icon } from 'antd';
import {Link} from "react-router-dom";
import {observer, inject} from "mobx-react";
import {Redirect} from "react-router-dom";
import I18nText from "../../components/i18nText/I18nText";
import "./appLayout.pcss";

const { Header, Content, Footer } = Layout;
const { SubMenu } = Menu;

class AppLayout extends Component{
    onSelectedMenu(selected){
        switch (selected.key) {
            case "user.logout": this.props.store.logout();
        }
    }
    render() {
        if(this.props.store.userService.user){
            return (
                <Layout className="layout app-layout">
                    <Header>
                        <div className="logo" />
                        <Menu
                            className="site-menu"
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={[this.props.pageIndex]}
                            selectedKeys={[this.props.pageIndex]}
                            onSelect={this.onSelectedMenu.bind(this)}
                        >
                            <Menu.Item key="1"><Link to="/"><I18nText textKey="Home"/></Link></Menu.Item>
                            <Menu.Item key="2"><Link to="/users"><I18nText textKey="Users"/></Link></Menu.Item>
                            <SubMenu
                                key="4"
                                className="float-right"
                                title={
                                    <span>
                                        <Icon type="global" />
                                    </span>
                                }
                            >
                                {this.props.store.locales.map(locale=> <Menu.Item onClick={event=> {this.props.store.setLocale(locale.locale)}} key={"locale."+locale.locale}>{locale.flag} {locale.name}</Menu.Item>)}
                            </SubMenu>
                            <SubMenu
                                key="3"
                                className="float-right"
                                title={
                                    <span>
                                        <Icon type="user" />
                                        {this.props.store.userService.user.firstName}
                                    </span>
                                }
                            >
                                <Menu.Item key="user.profile"><I18nText textKey="Profile"/></Menu.Item>
                                <Menu.Item key="user.logout"><I18nText textKey="Logout"/></Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Header>
                    <Content className="site-content">
                        <Breadcrumb className="breadcrumb-container">
                            {this.props.breadcrumbs.map(crumb=> <Breadcrumb.Item key={crumb.key}>{crumb.element}</Breadcrumb.Item>)}
                        </Breadcrumb>
                        <div className="content-container">
                            {this.props.children}
                        </div>
                    </Content>
                    <Footer className="site-footer">Hapi-React boilerplate</Footer>
                </Layout>
            );
        }
        else{
            return <Redirect to="/login"/>
        }
    }
}
AppLayout = inject("store")(observer(AppLayout));

export default AppLayout;
