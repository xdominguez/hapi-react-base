import _ from "lodash";
import {env} from "../env";

export class EntityService {
    constructor(updateToken){
        this.updateToken = updateToken;
    }

    setUser(user){
        this.user = user;
    }

    setHistory(history){
        this.history = history;
    }

    setNotification(notifier){
        this.notifier = notifier;
    }

    request(requestParams){
        const self = this;
        return new Promise(function (resolve, reject) {
            let fetchOptions = {
                method: requestParams.method,
                headers: {}
            };
            if(requestParams.requireAuth && self.user){
                fetchOptions.headers.Authorization = "Bearer "+self.user.token;
            }
            if(requestParams.method === "POST" || requestParams.method === "PUT"){
                if(requestParams.body){
                    fetchOptions.headers["Content-Type"] = "application/json";
                    fetchOptions.body = JSON.stringify(requestParams.body);
                }

            }
            fetch(env.apiUrl + requestParams.url, fetchOptions)
                .then(response => {
                    if(response.status === 204){
                        return response.text();
                    }
                    else if(response.status >= 200 && response.status < 300){
                        return response.json();
                    }
                    else{
                        if(response.status === 401){
                            self.updateToken("");
                        }
                        else{
                            console.error("error", response);
                        }
                        reject(response);
                    }
                })
                .then(data => {
                    if(data){
                        if(_.isFunction(requestParams.transformData)){
                            data = requestParams.transformData(data);
                        }
                    }
                    resolve(data);
                }).catch(error => {
                    console.log("error", error);
                    reject(error);
                });
        })
    }
}
