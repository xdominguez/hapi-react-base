import React, {Component} from "react";
import "./userForm.pcss";
import {observer, inject} from "mobx-react";
import {Button, Col, Form, Input, Row, Select} from "antd";
import I18nText from "../../components/i18nText/I18nText";
import _ from "lodash";

const { Option } = Select;
class UserForm extends Component{
    constructor(props, context) {
        super(props);
        this.state = {
            loading: false,
            confirmDirty: false
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({
                    loading: true
                });
                if(this.props.user.id){
                    values.id = this.props.user.id;
                    this.props.store.userService.update(values).then(resp => {
                        this.setState({
                            loading: false
                        });
                        if(_.isFunction(this.props.onUserSaved)){
                            this.props.onUserSaved(values);
                        }
                    });
                }
                else{
                    this.props.store.userService.create(values).then(user => {
                        this.setState({
                            loading: false
                        });
                        this.props.form.setFieldsValue({
                            firstName: "",
                            lastName: "",
                            email: "",
                            role: 2,
                            password: "",
                            confirmPassword: ""
                        });
                        if(_.isFunction(this.props.onUserSaved)){
                            this.props.onUserSaved(user);
                        }
                    });
                }
            }
        });
    };
    compareToFirstPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    };
    validateToNextPassword = (rule, value, callback) => {
        const { form } = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirmPassword'], { force: true });
        }
        callback();
    };
    handleConfirmBlur = e => {
        const { value } = e.target;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    };

    render(){
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="user-form">
                <Form onSubmit={this.handleSubmit} className="user-create-form">
                    <Row type="flex" justify="center">
                        <Col md={6} sm={22} className="field-left">
                            <Form.Item>
                                {getFieldDecorator('firstName', {
                                    rules: [{ required: true, message: this.props.store.getMessage("Please input your name") }],
                                    initialValue: this.props.user.firstName
                                })(
                                    <Input
                                        placeholder={this.props.store.getMessage("Name")}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col md={6} sm={22}>
                            <Form.Item>
                                {getFieldDecorator('lastName', {
                                    rules: [{ required: true, message: this.props.store.getMessage("Please input your last name") }],
                                    initialValue: this.props.user.lastName
                                })(
                                    <Input
                                        placeholder={this.props.store.getMessage("Last name")}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row type="flex" justify="center">
                        <Col md={6} sm={22} className="field-left">
                            <Form.Item>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: this.props.store.getMessage("Please input your email") }],
                                    initialValue: this.props.user.email
                                })(
                                    <Input
                                        placeholder={this.props.store.getMessage("Email")}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col md={6} sm={22}>
                            <Form.Item>
                                {getFieldDecorator('role', {
                                    rules: [{ required: true, message: this.props.store.getMessage("Please input your email") }],
                                    initialValue: this.props.user.role || 2
                                })(
                                    <Select>
                                        <Option value={1}>{this.props.store.getMessage("Administrator")}</Option>
                                        <Option value={2}>{this.props.store.getMessage("Owner")}</Option>
                                    </Select>
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    {!this.props.user.id?<Row type="flex" justify="center">
                        <Col md={6} sm={22} className="field-left">
                            <Form.Item hasFeedback>
                                {getFieldDecorator('password', {
                                    rules: [
                                        { required: true, message: this.props.store.getMessage("Please input your password") },
                                        { type: "string", min: 8, message: this.props.store.getMessage("Password must be at least 8 characters") },
                                        {
                                            validator: this.validateToNextPassword,
                                        },
                                    ]
                                })(
                                    <Input
                                        type="password"
                                        placeholder={this.props.store.getMessage("Password")}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                        <Col md={6} sm={22}>
                            <Form.Item hasFeedback>
                                {getFieldDecorator('confirmPassword', {
                                    rules: [
                                        { required: true, message: this.props.store.getMessage("Please confirm your password") },
                                        {
                                            validator: this.compareToFirstPassword,
                                        },
                                    ],
                                })(
                                    <Input
                                        type="password"
                                        placeholder={this.props.store.getMessage("Confirm password")}
                                        onBlur={this.handleConfirmBlur}
                                    />,
                                )}
                            </Form.Item>
                        </Col>
                    </Row>:null}
                    <Row type="flex" justify="center">
                        <Col md={12} sm={22}>
                            <Button loading={this.state.loading} type="primary" htmlType="submit" className="login-form-button float-right">
                                <I18nText textKey="Save"/>
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        );
    }
}

UserForm = inject("store")(observer(UserForm));

const UserFormPage = Form.create({ name: 'user_form' })(UserForm);
export default UserFormPage;
