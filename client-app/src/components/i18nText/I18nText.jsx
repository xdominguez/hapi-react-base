import React, {Component} from "react";
import {observer, inject} from "mobx-react";

class I18nText extends Component{
    render(){
        return this.props.store.getMessage(this.props.textKey);
    }
}
I18nText = inject("store")(observer(I18nText));

export default I18nText;
