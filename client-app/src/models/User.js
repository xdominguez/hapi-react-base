import moment from "moment";

export class User {
    static ADMIN = 1;
    static OWNER = 2;

    constructor(user) {
        this._id = user.id;
        this._firstName = user.firstName;
        this._lastName = user.lastName;
        this._email = user.email;
        this._role = user.role;
        this._photo = user.photo;
        this._createdAt = moment(user.createdAt);
        this._updatedAt = moment(user.updatedAt);
        this._password = user.password;
        this._user = user;
    }

    json(){
        return {
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            role: this.role,
            photo: this.photo,
            createdAt: this.createdAt.toISOString(),
            updatedAt: this.updatedAt.toISOString(),
            password: this.password
        }
    }

    isAdmin(){
        return this.role === User.ADMIN;
    }
    isOwner(){
        return this.role === User.OWNER;
    }

    get user() {
        return this._user;
    }

    set user(value) {
        this._user = value;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get role() {
        return this._role;
    }

    set role(value) {
        this._role = value;
    }

    get photo() {
        return this._photo;
    }

    set photo(value) {
        this._photo = value;
    }

    get createdAt() {
        return this._createdAt;
    }

    set createdAt(value) {
        this._createdAt = moment(value);
    }

    get updatedAt() {
        return this._updatedAt;
    }

    set updatedAt(value) {
        this._updatedAt = moment(value);
    }

    get password() {
        return this._password;
    }

    set password(value) {
        this._password = value;
    }
}
