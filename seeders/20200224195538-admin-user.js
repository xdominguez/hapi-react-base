'use strict';
const bcrypt = require('bcrypt');
const uuid = require("uuid");

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      id: uuid.v1(),
      firstName: 'Admin',
      lastName: 'Super',
      email: 'admin@test.com',
      createdAt: new Date(),
      updatedAt: new Date(),
      password: bcrypt.hashSync('admin123', 12),
      role: 1
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
