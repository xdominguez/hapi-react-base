'use strict';

const Hapi = require('@hapi/hapi');
const Path = require('path');
const Sequelize = require('sequelize');
const loadApiRoutes = require("./routes");
const envConfig = require("./config/config");
const env = process.env.NODE_ENV || 'development';
const userService = require("./services/userService");

const validate = async function (decoded, request, h) {
    const user = await userService.get(decoded.id);
    return { isValid: user !== null };
};

const initServer = async () => {
    const sequelize = new Sequelize(envConfig[env].database, envConfig[env].username, envConfig[env].password, {
        host: envConfig[env].host,
        dialect: 'mysql'
    });
    await sequelize.authenticate();
    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: true,
            files: {
                relativeTo: Path.join(__dirname, 'client-app/build')
            }
        }
    });

    await server.register(require('hapi-auth-jwt2'));
    server.auth.strategy('jwt', 'jwt',
        { key: envConfig[env].secret,
            validate
        });

    server.auth.default('jwt');

    await server.register(require('inert'));

    server.ext('onPreResponse', (request, h) => {
        const {response} = request;
        if(response.isBoom && response.output.statusCode === 404){
            return h.file('index.html');
        }
        return h.continue;
    });
    loadApiRoutes(server);
    server.route({
        method: 'GET',
        path: '/{param*}',
        config: { auth: false },
        handler: {
            directory: {
                path: '.',
                index: "index.html"
            }
        }
    });

    return server;
};

exports.init = async () => {
    const server = await initServer();
    await server.initialize();
    return server;
};

exports.start = async () => {
    const server = await initServer();
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});
