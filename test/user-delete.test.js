'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server.js');

describe('Delete user - DELETE /api/user/{id}', () => {
    let server;
    let token;
    let user;

    beforeEach(async () => {
        server = await init();
        const res = await server.inject({
            method: 'post',
            url: '/api/login',
            headers: {
                "Content-Type:": "application/json"
            },
            payload: {"email":"admin@test.com","password":"admin123"}
        });
        token = res.result.token;

        const userRes = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=createdAt&sortOrder=descend',
            headers: {
                "Content-Type:": "application/json",
                Authorization: `Bearer ${token}`
            }
        });
        user = userRes.result.list[0];
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 401 when deletes a user', async () => {
        const res = await server.inject({
            method: 'delete',
            url: `/api/user/${user.id}`,
            headers: {
                Authorization: `Bearer fsdfsd`
            }
        });
        expect(res.statusCode).to.equal(401);
    });

    it('responds with 404 when deletes a user', async () => {
        const res = await server.inject({
            method: 'delete',
            url: `/api/user/sdfsdfd`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(404);
    });

    it('responds with 204 when deletes a user', async () => {
        const res = await server.inject({
            method: 'delete',
            url: `/api/user/${user.id}`,
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(204);
    });
});
