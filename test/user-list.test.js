'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server.js');

describe('List user - GET /api/user', () => {
    let server;
    let token;

    beforeEach(async () => {
        server = await init();

        const res = await server.inject({
            method: 'post',
            url: '/api/login',
            headers: {
                "Content-Type:": "application/json"
            },
            payload: {"email":"admin@test.com","password":"admin123"}
        });
        token = res.result.token;
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=firstName&sortOrder=ascend',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 400', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=-1&perPage=10&sortField=firstName&sortOrder=ascend',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=-10&sortField=firstName&sortOrder=ascend',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=wrong&sortOrder=ascend',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=firstName&sortOrder=asc',
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 401', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/api/user?page=1&perPage=10&sortField=firstName&sortOrder=ascend',
            headers: {
                Authorization: "Bearer eyJhbGciOiJIUzI1IsInR5cCI6IkpXVCJ9.eyJpZCI6IjM2YjkyZGEwLTZiMmMtMTFlYS1iNTJmLWJiY2U3ZTQwZTViMyIsImlhdCI6MTU4ODQ3MDkyOH0.BqRF0bVDDeq7C4Za1ZV_Pm_1V4_zcmURoKxDbgnx_9o"
            }
        });
        expect(res.statusCode).to.equal(401);
    });
});
