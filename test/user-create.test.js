'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server.js');
const Util = require("../util/utilFunctions");

describe('Create user - POST /api/user', () => {
    let server;
    let token;

    beforeEach(async () => {
        server = await init();

        const res = await server.inject({
            method: 'post',
            url: '/api/login',
            headers: {
                "Content-Type:": "application/json"
            },
            payload: {"email":"admin@test.com","password":"admin123"}
        });
        token = res.result.token;
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 201 when creates a user', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":2,
                "password":"12345678",
                "confirmPassword":"12345678"
            }
        });
        expect(res.statusCode).to.equal(201);
    });

    it('responds with 401 when token is not valid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXJ9.eyJpZCI6IjM2YjkyZGEwLTZiMmMtMTFlYS1iNTJmLWJiY2U3ZTQwZTViMyIsImlhdCI6MTU4ODQ3MDkyOH0.BqRF0bVDDeq7C4Za1ZV_Pm_1V4_zcmURoKxDbgnx_9o"
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":2,
                "password":"12345678",
                "confirmPassword":"12345678"
            }
        });
        expect(res.statusCode).to.equal(401);
    });

    it('responds with 400 when password and its confirmation don\'t match', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":2,
                "password":"12345678",
                "confirmPassword":"1234567"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400 when password length is less than 8', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":2,
                "password":"1234567",
                "confirmPassword":"1234567"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400 when email is not valid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":2,
                "password":"12345678",
                "confirmPassword":"12345678"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it('responds with 400 when role is not valid', async () => {
        const id = Util.getRandomString(10);
        const name = `Pepe-${id}`;
        const email = `Pepe-${id}@gmail.com`;
        const res = await server.inject({
            method: 'post',
            url: '/api/user',
            headers: {
                Authorization: `Bearer ${token}`
            },
            payload: {
                "firstName":name,
                "lastName":"Perez",
                "email":email,
                "role":3,
                "password":"12345678",
                "confirmPassword":"12345678"
            }
        });
        expect(res.statusCode).to.equal(400);
    });
});
