module.exports = function (h) {
    const response = h.response();
    response.statusCode = 204;
    return response;
};
